﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cave;



public class FlyTowardsSpectator : CollisionSynchronization /*MonoBehaviour*/ {

	float speed = 4;
	float z = 10;
	float x = 1f;
	float t = 80;

	bool destroy = false;

	public FlyTowardsSpectator()
		: base(new[] { Cave.EventType.OnCollisionEnter })
	{

	}
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		z = z - speed * TimeSynchronizer.deltaTime;
		if (!destroy) {
			transform.position = new Vector3 (1f, 0.0f, z);
		}
			
		if (destroy) {
		x = x - speed * TimeSynchronizer.deltaTime;
			transform.position = new Vector3(x, 0.0f, z);
		}

		if (t < -80) {
			Destroy (this.gameObject);
		}
		t--;
	}

	public override void OnSynchronizedCollisionEnter(GameObject other) {
		Debug.Log (other.name);
		if (other.name == "Lasersword") {
			destroy = true;
			Debug.Log ("Blöööööööd");
		}	
	}

	/*public void OnCollisionEnter(Collision other) {
		Debug.Log (other.gameObject.name);
		if (other.gameObject.name == "Lasersword") {
			destroy = true;
			Debug.Log ("Blöööööööd");
		}	
	}*/

}
